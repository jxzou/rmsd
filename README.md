# RMSD

This is a simple utility designed to compare the geometries of two molecules. The Kabsch algorithm is applied here. To compile it, just run (Intel compiler recommended)

    ifort calc_rmsd.f90 -mkl -o calc_rmsd

To use it, just run

    ./calc_rmsd a.gjf b.gjf

then the RMSD value of all atoms are calculated. If you only want to compare some atoms in two gjf files, e.g. atoms 1-5 in a.gjf, atoms 2-6 in b.gjf. You can run

    ./calc_rmsd a.gjf b.gjf 1-5 2-6
    
The range must be specified with a '-' symbol. Then RMSD value for atoms 1-5 v.s. 2-6 is calculated.
Other uncompared atoms will be translated and rotated according to the new orientation of compared atoms.
A b_new.gjf file will be generated. This is just the new geometry of b.gjf which has maximum overlap with a.gjf
(a.gjf is fixed).

Note1: the element symbols are not checked during calculating the RMSD value. Thus you should be careful if you
are dealing with two molecules which have different elements.

Note2: only the Gaussian gjf format is supported.

Note3: the atom-atom correspondence must be assured by users. This utility is unable to deal with disordered atoms in two files.